from selenium import webdriver
from selenium.webdriver.common.keys import Keys

#browser = Firefox(options=opts)
browser = webdriver.Firefox()
browser.get('https://duckduckgo.com/')

search_form = browser.find_element_by_id('search_form_input_homepage')
search_form.send_keys('real python')
search_form.submit()
try:
    results = browser.find_elements_by_class_name("result")
    print(results)
except Exception as e:
    print("error : {0}".format(str(e)))
browser.close()