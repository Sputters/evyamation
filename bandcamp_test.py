from selenium import webdriver
from selenium.webdriver.common.keys import Keys

#browser = Firefox(options=opts)
browser = webdriver.Firefox()
browser.get('https://bandcamp.com')

try:
    browser.find_element_by_class_name('playbutton').click()
    tracks = browser.find_elements_by_class_name('discover-item')
    print(len(tracks))
    next_button = [e for e in browser.find_elements_by_class_name('item-page')
        if e.text.lower().find('next') > -1]
    next_button[0].click()
except Exception as e:
    print("error : {0}".format(str(e)))

browser.close()